<?php
namespace BF\PhpUtils;

class ExceptionHandler
{
	public static $production = true;

	public static function register()
	{
		set_exception_handler(array(__CLASS__,"handleException"));
	}

	public static function handleException($exception)
	{
		$key = 0;
		// these are our templates
        $tracelineTemplate = "#%s %s(%s): %s(%s)";
        $msgTemplate = "PHP Fatal error:  Uncaught exception '%s' with message '%s' in %s:%s\nStack trace:\n%s\n\tthrown in %s on line %s";

	    // alter your trace as you please, here
	    $trace = $exception->getTrace();
        self::normalizeArguments($trace);

        // build your tracelines
	    $tracelines = array();
	    foreach ($trace as $key => $stackEntry) {
            $tracelines[] = self::buildTraceLine($tracelineTemplate,$key,$stackEntry);
	    }
	    // trace always ends with {main}
        $tracelines[] = '#' . ++$key . ' {main}';

	    if (self::$production) $tracelines = array(" - trace hidden - ");

	    // write tracelines into main template
	    $msg = self::buildFinalMessage($msgTemplate,$exception,$tracelines);

	    if (php_sapi_name()!="cli") {
	    	$msg = str_replace("\n","<br>",$msg);
	    	$msg = str_replace("\t","&nbsp;&nbsp;&nbsp;&nbsp;",$msg);
	    	$msg = '<pre style="background-color: white; border: double 3px black; color: black; padding: 5px; font-family: arial; font-size:13px;">'.$msg."</pre>";
	    }

	    // log or echo as you please
	    echo $msg;
	    exit;
	}

    /**
     * @param string $msgTemplate
     * @param string[] $traceLines
     */
    protected static function buildFinalMessage($msgTemplate,$exception,$traceLines)
    {
        return sprintf(
            $msgTemplate,
            get_class($exception),
            $exception->getMessage(),
            (self::$production) ? " - hidden - " : $exception->getFile(),
            (self::$production) ? " - hidden - " : $exception->getLine(),
            implode("\n", $traceLines),
            (self::$production) ? " - hidden - " : $exception->getFile(),
            (self::$production) ? " - hidden - " : $exception->getLine()
        );
    }

    /**
     * @param string $tracelineTemplate
     */
    protected static function buildTraceLine($tracelineTemplate,$key,$stackEntry)
    {
        return sprintf(
            $tracelineTemplate,
            $key,
            isset($stackEntry['file']) ? $stackEntry['file'] : " - unknown - ",
            isset($stackEntry['line']) ? $stackEntry['line'] : " - unknown - ",
            $stackEntry['function'],
            implode(', ', $stackEntry['args'])
        );
    }

    /**
     * @param $trace
     */
    protected static function normalizeArguments(&$trace)
    {
        foreach ($trace as $key => $stackPoint) {
            // I'm converting arguments to their type
            // (prevents passwords from ever getting logged as anything other than 'string')
            if (isset($stackPoint['args'])) {
                $trace[$key]['args'] = array_map('gettype', $trace[$key]['args']);
            } else {
                $trace[$key]['args'] = array();
            }
        }
    }
}
