<?php
namespace BF\PhpUtils;

class ValidationException extends \Exception 
{
	protected $fieldErrors = array();
	protected $targetPath = null;
	
	public function addFieldError($name,$message=true) 
	{
		$this->fieldErrors[$name] = $message;
		$this->message .= "$name: $message; ";
	}
	public function getFieldErrors()
	{
		return $this->fieldErrors;
	}

	public function setTargetPath($path)
	{
		$this->targetPath = $path;
	}
	public function getTargetPath()
	{
		return $this->targetPath;
	}
}
