<?php
namespace BF\PhpUtils;

class AlphaId 
{
    public $passKey = null;
    public $defaultIndex = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function Encode($in)
    {
        $index = $this->generateIndex();
        $base  = strlen($index);

        $out = "";
        for ($t = floor(log($in, $base)); $t >= 0; $t--) {
            $bcp = bcpow($base, $t);
            $a   = floor($in / $bcp) % $base;
            $out = $out . substr($index, $a, 1);
            $in  = $in - ($a * $bcp);
        }
        $out = strrev($out); // reverse

        return $out;
    }

    public function Decode($in)
    {
        $index = $this->generateIndex();
        $base  = strlen($index);
        $in  = strrev($in);
        $out = 0;
        $len = strlen($in) - 1;
        for ($t = 0; $t <= $len; $t++) {
            $bcPow = bcpow($base, $len - $t);
            $out   = $out + strpos($index, substr($in, $t, 1)) * $bcPow;
        }

        $out = sprintf('%F', $out);
        $out = substr($out, 0, strpos($out, '.'));

        return $out;
    }

    private function generateIndex()
    {
        $index = $this->defaultIndex;
        $indexLength = strlen($index);

        if ($this->passKey !== null) {
            $i = str_split($index);

            $pass_hash = hash('sha512',$this->passKey);

            $p = array();
            for ($n = 0; $n < $indexLength; $n++) {
                $p[] =  substr($pass_hash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode('',$i);
        }

        return $index;
    }
} 