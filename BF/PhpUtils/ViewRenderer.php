<?php
namespace BF\PhpUtils;

class ViewRenderer
{
	const DEFAULT_FRAME_NAME = "PageFrame";

	protected $templateBaseDirectory;
	protected $name;
	protected $vars = array();
	protected $frameName = null;
	protected $viewClassBaseNamespace = "\\";
	protected $templateFile;

    /**
     * PHP-DI Container - if available/set it would be used to create controller/view classes
     * @var \DI\Container
     */
    protected $diContainer;

    /**
     * @param \DI\Container $container
     */
    public function setDIContainer(\DI\Container $container)
    {
        $this->diContainer = $container;
    }

    public function __construct()
	{
		$this->setDefaultFrameName(null);
	}

	public function setTemplateBaseDirectory($dir)
	{
		$this->templateBaseDirectory = $dir;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function setDefaultFrameName($frameName)
	{
		$this->frameName = !$frameName ? self::DEFAULT_FRAME_NAME : $frameName;
	}
	public function getDefaultFrameName()
	{
		return $this->frameName;
	}

	public function setViewClassBaseNamespace($ns)
	{
		$this->viewClassBaseNamespace = $ns;
		if (!str_begins_with($this->viewClassBaseNamespace, "\\")) $this->viewClassBaseNamespace = "\\".$this->viewClassBaseNamespace;
		if (!str_ends_with($this->viewClassBaseNamespace, "\\")) $this->viewClassBaseNamespace .= "\\";
	}

	public function setVars($vars)
	{
		$this->vars = $vars;
	}
	public function addVar($name,$value)
	{
		$this->vars[$name] = $value;
	}

    public function addVars($vars)
    {
        $this->vars = array_merge($this->vars,$vars);
    }

	public function render()
	{
		$slots = array();
		$content = $this->loadPage($this->name,$this->vars);
		$slotFiles = FileSystem::getFiles($this->templateBaseDirectory,'/'.preg_quote($this->name,'/').'-.+\\.php$/i');
		foreach ($slotFiles as $slotFile) {
			$slotFile = substr(basename($slotFile),0,strlen(basename($slotFile))-4);
			$slotName = substr($slotFile,strlen($this->name)+1);
			$slots[$slotName] = $this->loadPage($slotFile,$this->vars);
		}

		if ($this->frameName) {
			$vars = array_merge($this->vars,array("content"=>$content,"slots"=>$slots,"name" => $this->name));
			$finalContent = $this->loadPage($this->frameName,$vars);
		}else{
			$finalContent = $content;
		}
		return $finalContent;
	}

	protected function loadPage($__name__,&$vars=array())
	{
		$this->templateFile = str_replace('\\','/',$__name__);

        $validation_method = "validate";
        $http_method = null;
        $validation_method_with_http_method = null;
        if (isset($_SERVER["REQUEST_METHOD"])) {
            $http_method = strtolower($_SERVER["REQUEST_METHOD"]);
            $validation_method_with_http_method = $validation_method.ucfirst($http_method);
        }

		// load additional view class
		$className = $this->viewClassBaseNamespace.str_replace('/','\\',$this->templateFile);
		if (class_exists($className)) {
			$view = $this->getViewInstance($className);

			$view->setViewData($this->vars);
            $data = $this->callRequestMethodsOnView($view, $validation_method_with_http_method, $validation_method, $http_method);
            $vars = array_merge($vars,$data);

            $view->setTemplateFile($this->templateFile);
            $data = $view->render();

            if (is_array($data)) $vars = array_merge($vars,$data);
            $this->templateFile = $view->getTemplateFile();
		}

		return $this->renderFile($vars,$this->templateBaseDirectory,strtolower($this->templateFile).".php");
	}

    /**
     * @param string $className
     */
    protected function getViewInstance($className)
    {
        $viewInstance = $this->diContainer ? $this->diContainer->get($className) : new $className();
        if (!($viewInstance instanceof \BF\PhpUtils\View)) throw new \Exception(get_class($viewInstance)." isnt a subclass of \\BF\\PhpUtils\\View!");

        return $viewInstance;
    }

    /**
     * @param string $file
     */
    protected function renderFile($vars,$directory,$file)
    {
        $r = realpath($directory."/".$file);
        if (!$r) throw new \Exception($directory."/".$file." not found!");

        extract($vars);

        $old = error_reporting();
        $new = $old &~ E_NOTICE;
        error_reporting($new);
        ob_start();
        include(realpath("$directory/$file"));
        $c = ob_get_contents();
        ob_end_clean();
        error_reporting($old);
        return $c;
    }

    /**
     * @param View $view
     * @param string|null $validation_method_with_http_method
     * @param string $validation_method
     * @param string|null $http_method
     */
    protected function callRequestMethodsOnView($view, $validation_method_with_http_method, $validation_method, $http_method)
    {
        $returnValue = array();

        $returnValue = $this->callMethodOnObjectAnMergeResult($returnValue,$view,$validation_method_with_http_method);
        $returnValue = $this->callMethodOnObjectAnMergeResult($returnValue,$view,$validation_method);
        $returnValue = $this->callMethodOnObjectAnMergeResult($returnValue,$view,$http_method);

        return $returnValue;
    }

    protected function callMethodOnObjectAnMergeResult($vars,$object,$method)
    {
        if ($method && method_exists($object, $method)) {
            $r = call_user_func(array($object, $method));
            if (is_array($r)) $vars = array_merge($vars,$r);
        }
        return $vars;
    }
}
