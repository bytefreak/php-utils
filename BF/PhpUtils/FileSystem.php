<?php
namespace BF\PhpUtils;

class FileSystem
{
	/**
	 * checks if given path is absolute
	 * 
	 * @param string $path
	 * @return bool
	 */
	public static function isAbsolute($path) {
		$path = FileSystem::cleanPath ( $path );
		$isWindowsPath = preg_match ( '/^[a-z]:\\//i', $path );
		if (! $isWindowsPath) {
			return ( bool ) strlen ( $path ) > 0 && $path {0} == '/';
		}
		return ( bool ) $isWindowsPath;
	}

    /**
     * checks wether a given url with given scheme
     *
     * @param string $url
     * @param array $allowedSchemes
     * @return bool
     */
    public function isURL($url, $allowedSchemes = array("http","https")) {
		foreach ( $allowedSchemes as $scheme ) {
			if (str_begins_with ( $url, $scheme . "://" ))
				return true;
		}
		return false;
	}
	
	/**
	 * cleans up given path
	 * 
	 * @param string $path
	 * @return string
	 */
	public static function cleanPath($path)
    {
		$newPathComponents = array ();
        $path = str_replace ( "\\", "/", $path); // bugfix for windows paths
        $path = preg_replace ( "/\\/{2,}/", "/", $path);
        $pathComponents = explode ( '/', $path );
		
		foreach ( $pathComponents as $pathComponent ) {
			if ($pathComponent == ".." && count ($newPathComponents) > 0) {
				array_pop($newPathComponents);
			} elseif ($pathComponent != ".") {
                $newPathComponents[] = $pathComponent;
			}
		}
		
		$newPath = implode ( "/", $newPathComponents );
		$newPath = preg_replace ( "/\\/{2,}/", "/", $newPath );
		return $newPath;
	}
	
	/**
	 * created directories recursivly
	 * 
	 * @param string $sPath
	 * @param integer $mode
	 * @param integer $umask
	 */
	public static function mkPath($sPath, $mode = 0777, $umask = 0)
    {
		$sPath = str_replace ( "\\", "/", $sPath );
		
		$bResult = TRUE;
		if (! file_exists ( $sPath )) {
			$aPath = explode ( '/', $sPath );
			$aParentPath = $aPath;
			array_pop ( $aParentPath );
			$sParent = implode ( '/', $aParentPath );
			
			if (! file_exists ( $sParent )) {
				$bResult = FileSystem::mkPath ( $sParent, $mode, $umask );
			}
			if (! file_exists ( $sPath )) {
				$o = umask ( $umask );
				$b = dirname ( $sPath );
				if (! is_writable ( $b ))
					throw new \Exception ( "write access to " . $b . " denied!" );
				if (! mkdir ( $sPath, $mode )) {
					$bResult = FALSE;
				}
				umask ( $o );
			}
		}
		return $bResult;
	}
	
	/**
	 * deletes recursivly directory and files
	 * 
	 * @param string $sDir
	 */
	public static function delDir($sDir) {
		$bResult = TRUE;
		if (file_exists ( $sDir )) {
			$sCurrentDir = opendir ( $sDir );
			$sEntry = readdir ( $sCurrentDir );
			while ( $sEntry ) {
				if (is_dir ( "$sDir/$sEntry" ) && ($sEntry != "." && $sEntry != "..")) {
					if (! FileSystem::delDir ( $sDir . "/" . $sEntry ))
						return FALSE;
				} elseif ($sEntry != "." && $sEntry != "..") {
					if (! is_writable ( $sDir ))
						throw new \Exception ( "write access to $sDir denies!" );
					if (! unlink ( $sDir . "/" . $sEntry ))
						return FALSE;
				}
				$sEntry = readdir ( $sCurrentDir );
			}
			closedir ( $sCurrentDir );
			if (! rmdir ( $sDir ))
				return FALSE;
		}
		return $bResult;
	}
	
	/**
	 * returns file extension of given file
	 * 
	 * @param string $filename
	 * @return String|boolean
	 */
	public static function getExtension($filename)
    {
		$filename = basename ( $filename );
		if (substr_count ( $filename, "." ) == 0)
			return false;
		$foo = explode ( '.', $filename );
		return array_pop ( $foo );
	}
	
	/**
	 * copies sourcedirectory to targetdirectory recursivly
	 * 
	 * @param string $source
	 * @param string $target
	 * @param integer $mode
	 * @param integer $umask
	 */
	public static function copyDir($source, $target, $mode = 0777, $umask = 0)
    {
		if (! file_exists ( $source ))
			return;
		if (! file_exists ( $target ) && is_dir ( $source )) {
			FileSystem::mkPath ( $target, $mode, $umask );
		}
		if (is_dir ( $source ) && ! is_writable ( $target )) {
			throw new \Exception ( "target directory $target is not writable!!" );
		}
		
		if (is_dir ( $source )) {
			$reader = Dir ( $source );
			$e = $reader->read ();
			while ( $e ) {
				if ($e {0} == '.' || $e == 'CVS')
					continue;
				FileSystem::copyDir ( $source . '/' . $e, $target . '/' . $e, $mode, $umask );
				$e = $reader->read ();
			}
		} else {
			// datei kopieren
			copy ( $source, $target );
		}
	}
	
	/**
	 * returns directorynames in given directory
	 * 
	 * @param string $dirname
	 * @param array $excludes (dirnames (relative) to ignore - defaults to ., .. and .svn, CVS and .git)
	 * @param bool $absolute (paths returned are absolute?)
	 * @return array
	 */
	public static function getDirectories($dirname, $excludes = null, $absolute = true)
    {
		if (! is_array ($excludes)) $excludes = array ('.','..','CVS',".svn",".git");
		$directories = array ();
		if (! is_readable ( $dirname )) {
			return array();
		}
		$reader = Dir ( $dirname );
		if (! $reader)
			return array ();
		$e = $reader->read ();
		while ( $e ) {
			if (is_dir ( $dirname . '/' . $e ) && ! in_array ( $e, $excludes )) {
				$directories [] = ($absolute) ? $dirname . '/' . $e : $e;
			}
			$e = $reader->read ();
		}
		$reader->close ();
		return $directories;
	}
	
	/**
	 * returns all filenames in given directory
	 * 
	 * @param string $dir
	 * @param string $pattern (optional preg_match pattern which must match (with delimiter))
	 * @return array
	 */
	public static function getFiles($dir, $pattern = null)
    {
		$files = array ();
		if (! file_exists ( $dir ) || ! is_dir ( $dir ))
			return array ();
		if (! is_readable ( $dir )) {
			return array ();
		}
		$reader = Dir ( $dir );
		if (! is_object ( $reader ))
			return array ();
		$e = $reader->read ();
		while ( $e ) {
			if (! is_file ( $dir . '/' . $e )) {
				$e = $reader->read ();
				continue;
			}
			if (is_null($pattern) || preg_match($pattern,$e)) {
				$files [] = $dir . '/' . $e;
            }
			$e = $reader->read ();
		}
		return $files;
	}
	
	/**
	 * checks a directory if a finishing slash is present and adds one if not
	 * 
	 * @param string $dir
	 * @return string
	 */
	public static function setLastSlash($dir)
    {
		return rtrim($dir,'/').'/';
	}
	
	/**
	 * retrieves path relative to documentroot ($_SERVER[DOCUMENT_ROOT])
	 *
	 * - documentroot and item identified by $full must exist!
	 *
	 * @param string $full
	 * @return string
	 */
	public static function getWebPath($full, $realpath = true)
    {
        if (!isset($_SERVER["DOCUMENT_ROOT"])) throw new \Exception('DOCUMENT_ROOT not defined in $_SERVER!');

		$docroot = $_SERVER ["DOCUMENT_ROOT"];
        if ($realpath) $docroot = realpath($docroot);
		if (!FileSystem::isAbsolute($full)) $full = $realpath ? realpath(getcwd()."/$full") : getcwd () . "/$full";
		
		if (substr ( $full, 0, strlen ( $docroot ) ) != $docroot)
			return false;
		
		$webpath = '/' . FileSystem::getPathRelative ( $docroot, $full, $realpath );
		
		return $webpath;
	}

	/**
     * takes a filename with path and tries to find a unique, not already existent filename by adding a
     * sequential number before file extension
     *
	 * @param string $full
     * @return string
	 */
	public static function createUniqueFileName($full)
    {
		$foo = explode ( ".", $full );
		$ext = array_pop ( $foo );
		$full = implode ( ".", $foo );
		
		$file = $full . "." . $ext;
		$i = 1;
		while ( file_exists ( $file ) ) {
			$file = $full . "_" . sprintf ( "%02d", $i ) . "." . $ext;
			$i ++;
		}
		return $file;
	}
	
	/**
	 * creates an unique filename based upon the microtime without check for existing files
	 * 
	 * @param string $extension
	 * @param string $appendix (added after microtime, before file extension)
	 * @return string
	 */
	public static function createFilename($extension, $appendix = '')
    {
		$foo = str_replace ( " ", "_", @microtime ( true ) );
		return $foo . $appendix . '.' . $extension;
	}
	
	/**
	 * copies a file to another directory with keeping an unique filename
	 * 
	 * @param string $file
	 * @param string $dest
	 * @param string $newfilename
     * @param bool $unique
     *
	 * @return string filename (only basename in target directory)
	 */
	public static function copyFile($file, $dest, $newfilename = null, $unique = true)
    {
		if (! file_exists ( $file )) return false;
		if (! is_dir ( $dest )) FileSystem::mkPath ( $dest );
		$destfile = $newfilename ?: basename($file);

		if ($unique) {
			$full = FileSystem::createUniqueFilename ( $dest . $destfile );
		} else {
			$full = $dest . $destfile;
		}
		copy ( $file, $full );
		return basename ( $full );
	}
	
	/**
	 * moves a file to another directory with keeping an unique filename
	 * 
	 * @param string $file
	 * @param string $dest
	 * @return string basename of file
	 */
	public static function moveFile($file, $dest)
    {
		if (! file_exists ( $file )) return false;
		if (! is_dir ( $dest )) FileSystem::mkPath ( $dest );
		$full = FileSystem::createUniqueFilename ( $dest . "/" . basename ( $file ) );
		rename ( $file, $full );
		return basename ( $full );
	}

	/**
     * gets the relative path of a full base relative to a basepath
     *
	 * @param string $base the base path
     * @param string $path the fullpath to transform to relative path
     * @param bool $realpath wether to use realpath() before transforming
	 */
	public static function getPathRelative($base, $path, $realpath = true)
    {
		if (! FileSystem::isAbsolute ( $path )) $path = getcwd () . "/" . $path;
		if ($realpath) $path = realpath ( $path );
		if (is_dir ( $path )) $path .= "/";

		$path = FileSystem::cleanPath ( $path );
		$base = FileSystem::cleanPath ( $realpath ? realpath ( $base ) . "/" : $base . "/" );
		
		$webpath = str_replace ( $base, "", $path );
		if (strlen ( $webpath ) > 0 && $webpath {0} == "/") $webpath = substr ( $webpath, 1 );
		return $webpath;
	}

    /**
     * converts a filename with special chars to better filesystem friendly filenames
     *
     * @param $filename
     * @return string
     */
    public static function makeFilenameFilesystemCompatible($filename)
    {
		$filename = strtolower ( Types::toLatin1 ( $filename ) );
		$trans = array (
				chr ( 228 ) => 'ae',
				chr ( 196 ) => "ae",
				chr ( 246 ) => 'oe',
				chr ( 214 ) => "oe",
				chr ( 252 ) => 'ue',
				chr ( 220 ) => "ue",
				chr ( 223 ) => 'ss',
				chr ( 224 ) => 'a',
				chr ( 225 ) => 'a',
				chr ( 226 ) => 'a',
				chr ( 227 ) => 'a',
				chr ( 229 ) => 'a',
				chr ( 232 ) => 'e',
				chr ( 233 ) => 'e',
				chr ( 234 ) => 'e',
				chr ( 235 ) => 'e',
				chr ( 243 ) => 'o',
				chr ( 244 ) => 'o',
				chr ( 245 ) => 'o',
				chr ( 249 ) => 'u',
				chr ( 250 ) => 'u',
				chr ( 251 ) => 'u' 
		);
		$filename = strtr ( $filename, $trans );
		$filename = preg_replace ( '/[^a-z0-9\.]/', '_', $filename );
		$filename = preg_replace ( '/\s{2,}/', '_', $filename );
		$filename = preg_replace ( '/_{2,}/', '_', $filename );
		return $filename;
	}
	
	/**
	 * returns the filename from a filename with path string (basename, afer cleaning path)
	 * 
	 * @param string $full
	 * @return string
	 */
	public static function getFilename($full)
    {
		$full = FileSystem::cleanPath ( $full );
		return basename ( $full );
	}

    /**
     * execute a *nix-like "find" command for files and/or directories
     *
     * @param string $dir starting directpry
     * @param string $type (f or d or null for both)
     * @param string $pattern pattern to match filenames
     * @param integer $mindepth
     * @param integer $maxdepth
     * @param int $_level (internal use for recursion)
     * @return array
     */
    public static function find($dir, $type = null, $pattern = null, $mindepth = null, $maxdepth = null, $_level = 0)
    {
		$entries = array ();
		
		// erst dateien
		// aber nur, wenn schon mindepth erreicht
		if (($_level >= $mindepth || is_null($mindepth)) && (is_null($type) || $type == "f")) {
			$files = FileSystem::getFiles ( $dir, $pattern );
			foreach ( $files as $f )
				$entries [] = FileSystem::cleanPath ( $f );
		}
		
		if (($_level >= $mindepth || is_null($mindepth)) && (is_null($type) || $type == "d")) {
			$entries [] = FileSystem::cleanPath ( $dir );
		}
		
		if ($_level < $maxdepth || is_null($maxdepth)) {
			$dirs = FileSystem::getDirectories ( $dir );
			foreach ( $dirs as $e ) {
				$es = FileSystem::find ( $e, $type, $pattern, $mindepth, $maxdepth, $_level + 1 );
				foreach ( $es as $e2 ) {
					$entries [] = $e2;
                }
			}
		}
		return $entries;
	}
	
	/**
	 * check wether a given path is below a specific base path
	 *
	 * @param String $base        	
	 * @param String $path        	
	 * @param Boolean $useRealPath
     * @return bool
	 */
	public static function pathIsBelow($base, $path, $useRealPath = true)
    {
		if ($useRealPath) {
			$base = realpath ( $base );
			$path = realpath ( $path );
		}
		if (! $base || ! $path)
			return false;
		
		$base = FileSystem::cleanPath ( $base . "/" );
		$path = FileSystem::cleanPath ( $path . "/" );
		return (substr ( $path, 0, strlen ( $base ) ) == $base);
	}

	/**
	 * Returns TRUE if the $filename is readable, or FALSE otherwise.
	 * This
	 * function uses the PHP include_path, where PHP's is_readable() does not.
	 *
	 * @param string $filename        	
	 * @return boolean
	 */
	function isReadable($filename)
    {
		if (is_readable ( $filename )) {
			return true;
		}
		
		$path = get_include_path ();
		$dirs = explode ( PATH_SEPARATOR, $path );
		
		foreach ( $dirs as $dir ) {
			if ('.' == $dir) {
				continue;
			}
			
			if (is_readable ( $dir . DIRECTORY_SEPARATOR . $filename )) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * appends a string to the filename before the extension
     * @param string $filename
     * @param string $append
     * @return string
	 */
	function appendBeforeExtension($filename, $append)
    {
		$extension = FileSystem::getExtension ( $filename );
        if ($extension===false) {
            $r = $filename . $append;
        }else{
		    $filename_woExtension = substr ( $filename, 0, strlen ( $filename ) - strlen ( $extension ) - 1 );
            $r = $filename_woExtension . $append . '.' . $extension;
        }
		return $r;
	}
}