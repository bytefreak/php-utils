<?php
namespace BF\PhpUtils;

abstract class View
{
	protected $templateFile;
	protected $viewData = array();

    public function setViewDataItem($k,$v) { $this->viewData[$k] = $v; }
	public function setViewData($d) { $this->viewData = $d; }
	public function getViewData($k,$default=null)
	{
		return isset($this->viewData[$k]) ? $this->viewData[$k] : $default;
	}
	
	public function setTemplateFile($f) { $this->templateFile = $f; }
	public function getTemplateFile() { return $this->templateFile; }
	
	abstract function render();
}