<?php
namespace BF\PhpUtils;

class Types
{
	public static function int($val) { return ( (int) $val ); }
	public static function float($val,$locale=null)
	{
		if (is_null($locale)) return (float) $val;

		$old = setlocale(LC_NUMERIC,$locale);
		if (!$old) throw new \Exception("locale $locale not supported on this system!");

		$info = localeconv();
		$val = str_replace($info["thousands_sep"],"",$val);
		$val = str_replace($info["decimal_point"],".",$val);
		$val = (float) $val;

		setlocale(LC_NUMERIC,$old);
		return $val;
	}
	public static function double($val) { return ( (double) $val ); }
	public static function real($val) { return ( (real) $val ); }
	public static function bool($val)
	{
		if (strtolower(trim($val))==="true") $val = true;
		if (strtolower(trim($val))==="false") $val = false;
		if ($val===1) $val = true;
		if ($val===0) $val = false;
		return ( (bool) $val );
	}
	public static function string($val) { return ( (string) $val ); }
	public static function toArray($val) { return ( (array) $val ); }
	public static function toObject($val) { return ( (object) $val ); }

	public static function isUTF8($message)
	{
		// beschreibung der bitmuster
		$bytes = array();
		$bytes[1] = "0xxx xxxx";
		$bytes[2] = "110x xxxx";
		$bytes[3] = "1110 xxxx";
		$bytes[4] = "1111 0xxx";

		// die indikatoren beschreiben das erste byte, mit
		// dem die l�nge des zeichen in bytes bestimmt werden kann
		$indikators = $indikatorresults = array();
		foreach ($bytes as $length => $mask) {
			$mask = str_replace(" ","",$mask);

			$firstbyte = substr($mask,0,8);

			$result = bindec(str_replace("x","0",$firstbyte));
			$firstbyte = str_replace("0","1",$firstbyte);
			$firstbyte = str_replace("x","0",$firstbyte);
			$firstbyte = bindec($firstbyte);
//			echo $firstbyte."<br>";

			$indikators[$firstbyte] = $length;
			$indikatorresults[$firstbyte] = $result;
		}

		$messagebytes = array();
		for ($i=0; $i<strlen($message); $i++) {
			$messagebytes[] = ord($message[$i]);
		}

		$bytepos = 0;
		$bytelength = 0;
		foreach ($messagebytes as $k => $messagebyte) {

//			echo "<br>Byte: $k (".decbin($messagebyte)." --> ".chr($messagebyte).")<br>";

			// bytepos = 0 --> ein neues zeichen
			if ($bytepos==0) {
				$bytelength = 0;
				// l�nge des aktuellen zeichens ermitteln
				foreach ($indikators as $mask => $length) {
					$needresult = $indikatorresults[$mask];

					if (($messagebyte&$mask)==$needresult) {
						$bytelength = $length;
					}

//					echo decbin($messagebyte)." & ".decbin($mask)." = ".decbin($messagebyte & $mask)." (soll sein: ".decbin($needresult).")<br>";
				}
//				echo "L�nge des Zeichens: $bytelength<br>";

				// kein UTF8!
				if ($bytelength==0) return false;
			}else{

				// dieses byte muss an bit 6 eine 0 und bit 7 eine 1 stehen haben
				$mask = bindec("11000000");
				$needresult = bindec("10000000");

				if (($messagebyte&$mask)!=$needresult) return false;
			}

			$bytepos++;

			// n�chstes zeichen anspringen
			if ($bytepos==$bytelength) $bytepos = 0;
		}

		return true;
	}

	public static function toUTF8($m)
	{
		if (!Types::isUTF8($m)) $m = utf8_encode($m);
		return $m;
	}

	/**
	 * @return string
	 */
	public static function toLatin1($m)
	{
		if (Types::isUTF8($m)) $m = utf8_decode($m);
		return $m;
	}

	/**
	 * normalizer to a phone number like 0049172658493
	 * @param String $phone
	 * @param String $countrycode
	 * @return String
	 */
	public static function phonenumber($phone,$countrycode=null)
	{
		$phone = preg_replace("/^\\+/","00",$phone);
		$phone = preg_replace("/\\D/","",$phone);

		if (!is_null($countrycode) && !preg_match("/^00$countrycode/",$phone)) {
			if ($phone[0]=="0") {
				$phone{0} = " ";
				$phone = "00$countrycode".trim($phone);
			}
		}
		$phone = preg_replace("/^00/","+",$phone);
		return (string) $phone;
	}
}