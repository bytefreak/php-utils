<?php
namespace BF\PhpUtils\Image;

use Imagine\Image\Box;

class ImageTool 
{
	const IMAGING_SYSTEM_GD = "gd";
	const IMAGING_SYSTEM_IMAGICK = "imagick";
			
	protected $imagingSystem;
	protected $saveOptions = array("quality" => 80);
	/**
	 * @var \Imagine\Image\ImagineInterface
	 */
	protected $imagine;

    /**
     * @param string $system
     * @throws \Exception
     */
    public function __construct($system=self::IMAGING_SYSTEM_GD)
	{
		$this->imagingSystem = $system;
		switch ($system) {
			case self::IMAGING_SYSTEM_GD:
				$this->imagine = new \Imagine\Gd\Imagine();
				break;
			case self::IMAGING_SYSTEM_IMAGICK:
				$this->imagine = new \Imagine\Imagick\Imagine();
				break;
			default:
				throw new \Exception("System '$system' not supported!");				
		}
	}
	
	/**
	 * setting saving options: "quality" -> 0-100
	 * @param string $name
	 * @param mixed $value
	 */
	public function setSaveOption($name,$value)
	{
		$this->saveOptions[$name] = $value;
	}

    /**
     * scales an image from filesystem to a specific size (Aspect Fill Mode)
     * saves file to $targetFilename or returns binary data if null
     * @param string $filename
     * @param number $width
     * @param number $height
     * @param string $format
     * @param string $targetFilename
     * @return null|string
     */
	public function scaleAspectFill($filename, $width, $height, $format = "jpeg", $targetFilename=null)
	{
		$size = new Box($width,$height);
		
		$image = $this->imagine->open($filename);
		
		$scaleByX = $image->getSize()->getWidth()<$image->getSize()->getHeight();
		$ratio = $scaleByX ? $size->getWidth()/$image->getSize()->getWidth() : $size->getHeight()/$image->getSize()->getHeight();
		
		$newSize = $image->getSize()->scale($ratio);
		
		$resizedImage = $image->resize($newSize); /* @var $resizedImage \Imagine\Image\ImageInterface */
		unset($image);
		
		$data = null;
		if ($targetFilename) {			
			$resizedImage->save($targetFilename,$this->saveOptions);
		}else{
			$data = $resizedImage->get($format,$this->saveOptions);
		}
		unset($resizedImage);
		return $data;
	}
	
	/**
	 * @return \Imagine\Image\Box
	 * @param string $filename
	 */
	public function getSize($filename)
	{
		return $this->imagine->open($filename)->getSize();
	}
}