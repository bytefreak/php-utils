<?php
/**
 * @author thomas
 * @date 16.06.14-07:18
 */

namespace BF\PhpUtils;


class Container extends \ArrayObject
{
    public function __construct($array=array())
    {
        parent::__construct($array);
    }
    public function get($key,$default)
    {
        return $this->offsetExists($key) ? $this->offsetGet($key) : $default;
    }
    public function set($key,$value)
    {
        $this->offsetSet($key,$value);
    }
    function getByKeyPath($path, $default = null)
    {
        return array_get_by_keypath($this,$path,$default);
    }
    function setByKeyPath($path,$value)
    {
        array_set_by_keypath($this,$path,$value);
    }
}