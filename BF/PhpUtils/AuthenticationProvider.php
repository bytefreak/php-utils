<?php
namespace BF\PhpUtils;

class AuthenticationProvider {
	protected $realm;
	protected $forbiddenMessage;
	protected $authCallback;
	
	public function __construct($realm, $forbiddenMessage, $authCallback) {
		$this->realm = $realm;
		$this->forbiddenMessage = $forbiddenMessage;
		$this->authCallback = $authCallback;
	}
	
	public function handle() {
		$authFailed = true;
		
		if (isset ( $_SERVER ['PHP_AUTH_USER'] ) && isset ( $_SERVER ['PHP_AUTH_PW'] )) {
			$credentialsOK = call_user_func ( $this->authCallback, $_SERVER ["PHP_AUTH_USER"], $_SERVER ["PHP_AUTH_PW"] );
			$authFailed = ! $credentialsOK;
		}
		
		if ($authFailed) {
			header ( "WWW-Authenticate: Basic realm=\"$this->realm\"" );
			header ( "HTTP/1.0 401 Unauthorized" );
			echo $this->forbiddenMessage;
			exit();
		}
	}

}