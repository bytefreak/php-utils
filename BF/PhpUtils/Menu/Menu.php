<?php
namespace BF\PhpUtils\Menu;

class Menu extends \ArrayObject
{
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     * @param array $items
     */
    public function __construct($name,$items=array())
	{
        parent::__construct($items);
        $this->name = $name;
    }

    /**
     * @param callable $itemCallback --> function(idx,item,totalCount,isFirst,isLast,&$context){}
     * @param mixed &$context
     * @return string
     */
    public function renderWithCallback($itemCallback, &$context = null)
    {
        ob_start();

        $cnt = count($this);
        foreach ($this as $index => $item) {
            $r = $itemCallback($index,$item,$cnt,$index==0, $index==$cnt-1,$context);
            if ($r) {
                echo $r;
            }
        }

        return ob_get_clean();
    }

    /**
     * renders given item data using the provided template - use {{keyOfItemProperty}} to access properties
     * you can provide extra data which will be also available in template
     *
     * @param string $template
     * @param array $extraData
     * @return string
     */
    public function renderWithTemplate($template,$extraData=array())
    {
        ob_start();
        foreach ($this as $item) {
            $template = preg_replace_callback('/{{([^}]+)}}/',function($matches) use ($item,$extraData){
                $key = $matches[1];
                $r = "";
                if (isset($extraData[$key])) $r = $extraData[$key];
                if (is_object($item) && isset($item->$key)) $r = $item->$key;
                if (is_array($item) && isset($item[$key])) $r = $item[$key];
                return $r;
            },$template);
            echo $template;
        }
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}