<?php
/**
 * @author thomas
 * @date 29.09.14-18:25
 */
namespace BF\PhpUtils\Menu;

/**
 * Class MenuItem - a simple menu items as single container
 * @package BF\PhpUtils\Menu
 */
class MenuItem
{
    protected $data = array();

    function __get($name)
    {
        return $this->data[$name];
    }

    function __set($name, $value)
    {
        return $this->data[$name] = $value;
    }

    function __isset($name)
    {
        return isset($this->data[$name]);
    }

    function __unset($name)
    {
        unset($this->data[$name]);
    }
}