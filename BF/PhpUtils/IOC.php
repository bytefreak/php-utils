<?php
namespace BF\PhpUtils;

class IOC
{
	static $pimple;
	
	public static function init()
	{
		self::$pimple = new \Pimple();
	}
	
	public static function get($k)
	{
		return self::$pimple[$k];		
	}
	
	public static function set($k,$v)
	{
		self::$pimple[$k] = $v;
	}
}