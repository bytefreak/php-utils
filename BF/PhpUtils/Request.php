<?php
namespace BF\PhpUtils;

class Request
{
    /**
     * @var \BF\PhpUtils\Container
     */
    protected $_get = null;
    /**
     * @var \BF\PhpUtils\Container
     */
	protected $_post = null;
    /**
     * @var \BF\PhpUtils\Container
     */
	protected $_files = null;
    /**
     * @var \BF\PhpUtils\Container
     */
	protected $_request = null;
    /**
     * @var \BF\PhpUtils\Container
     */
	protected $_cookie = null;

	public function __construct ($importFromGlobals = true, $clearGlobals = true)
	{
        $get = array();
        $post = array();
        $request = array();
        $cookies = array();
        $files = array();

        if ($importFromGlobals) {
            $get = $this->strip($_GET);
            $post = $this->strip($_POST);
            $request = $this->strip($_REQUEST);
            $cookies = $this->strip($_COOKIE);
            $files = $this->strip($_FILES);
            if ($clearGlobals) {
                $_REQUEST = array();
                $_GET = array();
                $_POST = array();
                $_COOKIE = array();
                $_FILES = array();
            }
        }
        $this->importData($get,$post,$request,$cookies,$files);
	}

	private function strip ($a)
	{
		return get_magic_quotes_gpc() ? array_strip($a) : $a;
	}

	/**
	 * imports data from php request globals
	 */
	public function importData ($get,$post,$request,$cookie,$files)
	{
		$this->_request = new Container($request);
		$this->_post = new Container($post);
		$this->_get = new Container($get);
		$this->_cookie = new Container($cookie);
		$this->_files = new Container($files);
	}

	public function getFromPOST ($k, $default = null)
	{
		return $this->_post->get($k,$default);
	}

	public function existsInPOST ($k)
	{
		return isset($this->_post[$k]);
	}

	public function getFromGET ($k, $default = null)
	{
        return $this->_get->get($k,$default);
	}

	public function existsInGET ($k)
	{
		return isset($this->_get[$k]);
	}

	public function getFromCOOKIE ($k, $default = false)
	{
        return $this->_cookie->get($k,$default);
	}

	public function existsInCOOKIE ($k)
	{
		return isset($this->_cookie[$k]);
	}

	public function getFromFILES ($k, $default = false)
	{
        return $this->_files->get($k,$default);
	}

	public function existsInFILES ($k)
	{
		return isset($this->_files[$k]);
	}

	public static function getBasePath ()
	{
        $uri = $_SERVER["REQUEST_URI"];
        $uri = explode("?",$uri,2);
        $uri = $uri[0];
        return $uri;
	}

	/**
	 * @return string
	 */
	public static function getBaseUrl ()
	{
        $scheme = isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && strtolower($_SERVER["HTTP_X_FORWARDED_PROTO"])=="https" ||
        isset($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"])=="on" ? "https" : "http";
        $baseURL = $scheme."://".$_SERVER["HTTP_HOST"];
        return $baseURL;
	}

//	/**
//	 * determinates user agent
//	 * @static
//	 */
//	public static function isBot ()
//	{
//		static $bot = null;
//		if (isset($bot)) return $bot;
//
//		$botlist = array();
//		$botlist['Acoon'] = '/acoon\s+robot/i';
//		$botlist['AltaVista'] = '/(altavista|scooter)/i';
//		$botlist['Anzwers'] = '/anzwerscrawl/i';
//		$botlist['EuroSeek'] = '/arachnoidea/i';
//		$botlist['Excite'] = '/architextspider/i';
//		$botlist['PlanetSearch'] = '/fido/i';
//		$botlist['seednet'] = '/gais\s+robot/i';
//		$botlist['Google'] = '/googlebot/i';
//		$botlist['Northern Light'] = '/gulliver/i';
//		$botlist['Infoseek'] = '/(info|ultra)seek/i';
//		$botlist['Fireball'] = '/fireball/i';
//		$botlist['Search 4 Free'] = '/lwp-trivial/i';
//		$botlist['Lycos'] = '/lycos/i';
//		$botlist['EZResult'] = '/ezresult/i';
//		$botlist['Thunderstone'] = '/t-h-u-n-d-e-r-s-t-o-n-e/i';
//		$botlist['EuroFerret'] = '/muscatferret/i';
//		$botlist['Kolibri Online'] = '/kolibri/i';
//		$botlist['MSN Bot'] = '/msnbot/i';
//		$botlist['Hotbot'] = '/slurp/i';
//		$botlist['SwissSearch'] = '/swisssearch/i';
//		$botlist['WebCrawler'] = '/webcrawler/i';
//
//		$bot = false;
//		$userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
//		foreach ($botlist as $name => $reg) {
//			if (preg_match($reg, $userAgent)) {
//				$bot = $name;
//				break;
//			}
//		}
//		return $bot;
//	}

//	/**
//	 * Gets simple information about the user agent (name, version)
//	 *
//	 * @return Object
//	 */
//	function getUserAgent ()
//	{
//		static $user_agent = null;
//
//		if (isset($user_agent)) return $user_agent;
//
//		$user_agent = new \stdClass();
//
//		$agent = $_SERVER['HTTP_USER_AGENT'];
//		$products = array();
//
//		$pattern = "([^/[:space:]]*)"."(/([^[:space:]]*))?"."([[:space:]]*\\[[a-zA-Z][a-zA-Z]\\])?"."[[:space:]]*"."(\\((([^()]|(\\([^()]*\\)))*)\\))?"."[[:space:]]*";
//
//		while (strlen($agent)>0) {
//			$l = ereg($pattern, $agent, $a);
//			if ($l) {
//				// product, version, comment
//				array_push($products, array($a[1], $a[3], $a[6]));
//				$agent = substr($agent, $l);
//			} else {
//				$agent = '';
//			}
//		}
//
//		$found_agent = false;
//		// Directly catch these
//		foreach ($products as $product) {
//			switch ($product[0]) {
//				case 'Firefox':
//				case 'Netscape':
//				case 'Safari':
//				case 'Camino':
//				case 'Mosaic':
//				case 'Galeon':
//				case 'Opera':
//					$user_agent->name = $product[0];
//					$user_agent->version = $product[1];
//					$found_agent = true;
//					break;
//			}
//		}
//
//		if ($found_agent==false) {
//			// Mozilla compatible (MSIE, konqueror, etc)
//			if ($products[0][0]=='Mozilla'&&!strncmp($products[0][2], 'compatible;', 11)) {
//				$userAgent = array();
//				$cl = ereg("compatible; ([^ ]*)[ /]([^;]*).*", $products[0][2], $ca);
//				if ($cl) {
//					$user_agent->name = $ca[1];
//					$user_agent->version = $ca[2];
//				} else {
//					$user_agent->name = $products[0][0];
//					$user_agent->version = $products[0][1];
//				}
//			} else {
//				$user_agent->name = $products[0][0];
//				$user_agent->version = $products[0][1];
//			}
//		}
//		return $user_agent;
//	}

//	/**
//	 * retrieves all uploadfiles
//	 * @return UploadFiles[]
//	 */
//	public function getFiles ()
//	{
//		$files = array();
//		foreach ($this->getFileKeys() as $key) {
//			$files[$key] = $this->getFile($key);
//		}
//		return $files;
//	}
//
//	/**
//	 * retieve a upload file
//	 * @param String $requestname
//	 * @return UploadFile
//	 */
//	public function getFile ($requestname)
//	{
//		if (!$this->existsInFILES($requestname)) throw (new FileNotFoundException("file $requestname doesn't exists in request"));
//		$file = new UploadFile($this->getFromFILES($requestname));
//		return $file;
//	}

	/**
	 * @return boolean
	 */
	function isAjaxRequest ()
	{
		return array_key_exists('HTTP_X_REQUESTED_WITH', $_SERVER);
	}

	/**
	 * quick validation for non-empty/existant fields
	 * @param string[] $fields
	 * @return BOOL (false for failure)
	 */
	function validateNotEmpty($fields)
	{
		$ok = true;
		foreach ($fields as $fname) {
			if (!isset($this->_request[$fname]) || empty($this->_request[$fname])) {
				$ok = false;
				break;
			}
		}
		return $ok;
	}

    public static function get_remote_ip()
    {
        $ip = null;
        $cleanedServer = array();
        foreach ($_SERVER as $k => $v) {
            while (str_begins_with($k, "REDIRECT_",true)) $k = substr($k, strlen("REDIRECT_"));
            $cleanedServer[$k] = $v;
        }
        $remoteIpHeaderNames = array("HTTP_CLIENT_IP","HTTP_X_FORWARDED_FOR","HTTP_X_FORWARDED","HTTP_X_CLUSTER_CLIENT_IP","HTTP_FORWARDED_FOR","HTTP_FORWARDED","HTTP_VIA","REMOTE_ADDR");

        foreach ($remoteIpHeaderNames as $name) {
            if (isset($cleanedServer[$name])) {
                $ip = $cleanedServer[$name];
                break;
            }
        }
        if ($ip=="::1") $ip = "127.0.0.1";

        return $ip;
    }
}