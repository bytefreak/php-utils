<?php
namespace BF\PhpUtils;

class Dispatcher
{
    const VIEWDATA_URLPOSTFIX = "urlPostfix";

	protected $basePath;
	protected $pathToNameMap = array();
	/**
	 * @var \BF\PhpUtils\ViewRenderer
	 */
	protected $viewRenderer;

	protected $onPageFoundCallbacks = array();
	protected $onPageNotFoundCallbacks = array();

	public function setBasePath($bp) { $this->basePath = $bp=="/" ? "" : $bp; }
	public function getBasePath() { return $this->basePath; }

	public function __construct()
	{

	}

    /**
     * @param \BF\PhpUtils\ViewRenderer $renderer
     */
    public function setViewRenderer(\BF\PhpUtils\ViewRenderer $renderer)
	{
		$this->viewRenderer = $renderer;
	}

	public function addMapping($path,$name)
	{
		$this->pathToNameMap['/'.trim($path,'/')] = $name;
	}

	public function setOnPageFoundCallback($cb)
	{
		if (is_callable($cb)) $this->onPageFoundCallbacks[] = $cb;
	}
	public function setOnPageNotFoundCallback($cb)
	{
		if (is_callable($cb)) $this->onPageNotFoundCallbacks[] = $cb;
	}



	public function run($path,$frameName=null,$data=array())
	{
        $pathRequest = $this->requestPath('/'.trim($this->basePath ? str_replace($this->basePath, "", $path) : $path,'/'));

		if (!$pathRequest->isFound()) {
			header("HTTP/1.0 404 Not Found");

			foreach ($this->onPageNotFoundCallbacks as $cb) $cb(array("path" => $pathRequest->requestedPath));
            echo "Page not found!";
			exit;
		}

		$this->viewRenderer->setName($pathRequest->name);
		if (!is_null($frameName)) $this->viewRenderer->setDefaultFrameName($frameName);

		foreach ($this->onPageFoundCallbacks as $cb) {
			$cb(array("name" => $pathRequest->name, "frameName" => $this->viewRenderer->getDefaultFrameName(), "urlPostfix" => $pathRequest->urlPostFix, "path" => $pathRequest->requestedPath));
		}

		$this->viewRenderer->setVars(array(self::VIEWDATA_URLPOSTFIX=>$pathRequest->urlPostFix));
        $this->viewRenderer->addVars($data);

        $content = "";
		try {
			$content = $this->viewRenderer->render();
		}catch (ValidationException $validationException) {
            $this->handleValidationException($frameName, $data, $validationException);
		}
        echo $content;
	}

    /**
     * @param $relPath
     * @return PathRequest
     */
    protected function requestPath($relPath)
    {
        $pathRequest = new PathRequest();
        $pathRequest->requestedPath = $relPath;

        if (isset($this->pathToNameMap[$relPath])) {
            $pathRequest->name = $this->pathToNameMap[$relPath];
        } else {
            foreach ($this->pathToNameMap as $key => $targetName) {
                if (str_ends_with($key, "*")) {
                    $key = substr($key,0,strlen($key)-1);
                    if (str_begins_with($relPath, $key)) {
                        $pathRequest->name = $targetName;
                        $pathRequest->urlPostFix = substr($relPath, strlen($key));
                        break;
                    }
                }
            }
        }

        return $pathRequest;
    }

    /**
     * @param $frameName
     * @param $data
     * @param $validationException
     */
    protected function handleValidationException($frameName, $data, $validationException)
    {
        $target = $validationException->getTargetPath();
        if ($target) {
            $targetPath = $this->getBasePath() . "/" . $target;
            $this->run($targetPath, $frameName, array_merge($data, array("errors" => $validationException->getFieldErrors())));
        }
    }
}

/**
 * Class PathRequest
 * @package BF\PhpUtils
 * @private
 */
class PathRequest
{
    public $name;
    public $urlPostFix;
    public $requestedPath;

    public function isFound()
    {
        return !is_null($this->name);
    }
}