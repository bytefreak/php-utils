<?php
namespace BF\PhpUtils;


/**
 * Timer class to messure time
 * <code>
 * $timer = new Timer();
 * $timer->start();
 * $timer->stop();
 * echo $timer->getTime();
 * </code>
 */
class Timer
{
	var $startTime;
	var $stopTime;

	function __construct()
	{ 
		$this->start(); 
	}

	/**
	 * starts timer
	 */
	function start()
	{
		$this->startTime = microtime();
	}


	/**
	 * stops timer
	 */
	function stop()
	{
		$this->stopTime = microtime();
	}


	/**
	 * returns time in miliseconds
	 * @return Float
	 */
	function getTime()
	{
		$this->stop();
		return $this->elapsed($this->startTime, $this->stopTime);
	}


	function splitTime($echo = false)
	{
		$this->stop();
		if ($echo)
			echo $this->getTime();
		return $this->getTime();
	}

	/**
	 * @access protected
	 */
	function elapsed($a, $b)
	{
		list ($a_micro, $a_int) = explode(' ', $a);
		list ($b_micro, $b_int) = explode(' ', $b);

		if ($a_int > $b_int){
			return ($a_int - $b_int) + ($a_micro - $b_micro);
		}elseif ($a_int == $b_int) {
			if ($a_micro > $b_micro){
				return ($a_int - $b_int) + ($a_micro - $b_micro);
			}else if ($a_micro < $b_micro){
				return ($b_int - $a_int) + ($b_micro - $a_micro);
			}else{
				return 0;
			}
		}else{ // $a_int < $b_int
			return ($b_int - $a_int) + ($b_micro - $a_micro);
		}
	} 
} 
