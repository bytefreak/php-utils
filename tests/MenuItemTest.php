<?php
/**
 * @author thomas
 * @date 29.09.14-18:35
 */

namespace BF\PhpUtils\tests;

use BF\PhpUtils\Menu\MenuItem;

class MenuItemTest extends \PHPUnit_Framework_TestCase
{

    public function testInstance()
    {
        $mi = new MenuItem();
        $this->assertInstanceOf('\BF\PhpUtils\Menu\MenuItem',$mi);
    }

    public function testProperty()
    {
        $mi = new MenuItem();
        $name = "prop".sprintf("%02d",rand(1,50));

        $mi->$name = "xxx";

        $this->assertSame("xxx",$mi->$name);
        $this->assertObjectNotHasAttribute($name,$mi);

        $this->assertTrue(isset($mi->$name));
        $name2 = $name."xx";
        $this->assertFalse(isset($mi->$name2));

        unset($mi->$name);
        $this->assertFalse(isset($mi->$name));
    }
}
 