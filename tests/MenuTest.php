<?php
/**
 * @author thomas
 * @date 29.09.14-18:55
 */

namespace BF\PhpUtils\tests;


use BF\PhpUtils\Menu\Menu;
use BF\PhpUtils\Menu\MenuItem;

class MenuTest extends \PHPUnit_Framework_TestCase
{

    public function testInstance()
    {
        $m = new Menu("MenuName");
        $this->assertInstanceOf('\BF\PhpUtils\Menu\Menu',$m);
        $this->assertEquals("MenuName",$m->getName());
    }

    public function testWithItems()
    {
        $items = array(1,2,3);
        $m = new Menu("MenuName",$items);

        // testing counting
        $this->assertCount(count($items),$m);

        // testing array access
        $this->assertSame(2,$m[1]);

        // testing iteration
        foreach ($items as $k => $expected) {
            $this->assertSame($expected,$m[$k]);
        }
    }

    public function testRenderWithCallback()
    {
        $cb = function($idx,$item,$total,$isFirst,$isLast,&$context) {
            $context[] = array(
                'idx' => $idx,
                'item' => $item,
                'total' => $total,
                'isFirst' => $isFirst,
                'isLast' => $isLast
            );
        };

        $items = array("itemA","itemB","itemC");
        $m = new Menu("name",$items);

        $givenData = array();
        $m->renderWithCallback($cb,$givenData);
        $this->assertCount(3,$givenData);

        foreach ($items as $k => $v) {
            $this->assertSame($k,$givenData[$k]['idx']);
            $this->assertSame($v,$givenData[$k]['item']);
            $this->assertSame(count($items),$givenData[$k]['total']);
            $this->assertSame($k==0,$givenData[$k]['isFirst'],'isFirst');
            $this->assertSame($k==2,$givenData[$k]['isLast'],'isLast');
        }
    }

    public function testIndexCanBeAString()
    {
        $m = new Menu("name",array("key" => "value"));
        $this->assertSame("value",$m["key"]);
    }

    public function testRenderWithCallbackReturnsReturnValues()
    {
        $m = new Menu("Name",array("item"));
        $cb = function($idx,$item){ return "returned:".$item; };
        $v = $m->renderWithCallback($cb);
        $this->assertSame('returned:item',$v);
    }

    public function testRenderWithCallbackReturnsEchos()
    {
        $m = new Menu("Name",array("item"));
        $cb = function($idx,$item){ echo "returned:".$item; };
        $v = $m->renderWithCallback($cb);
        $this->assertSame('returned:item',$v);
    }

    public function testRenderWithTemplateWithoutVars()
    {
        $m = new Menu("name",array("item"));
        $x = $m->renderWithTemplate("test");
        $this->assertSame("test",$x);
    }

    public function testRenderWithTemplateWithExtraVars()
    {
        $m = new Menu("name",array("item"));
        $x = $m->renderWithTemplate("test-{{var}}",array("var" => "value"));
        $this->assertSame("test-value",$x);
    }

    public function testRenderWithTemplateWithScalarItem()
    {
        $m = new Menu("name",array("item"));
        $x = $m->renderWithTemplate("test-{{var}}");
        $this->assertSame("test-",$x);
    }
    public function testRenderWithTemplateWithArrayItem()
    {
        $m = new Menu("name",array(array("var" => "value")));
        $x = $m->renderWithTemplate("test-{{var}}");
        $this->assertSame("test-value",$x);
    }
    public function testRenderWithTemplateWithMenuItem()
    {
        $item = new MenuItem();
        $item->var = "value";
        $m = new Menu("name",array($item));
        $x = $m->renderWithTemplate("test-{{var}}");
        $this->assertSame("test-value",$x);
    }
}
 