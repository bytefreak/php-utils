<?php
function array_merge_recursive_simple($a, $b, $level = 0)
{
	// nicht arrays koennen wir nicht mergen
	// der zweite wert ueberschreibt dann den ersten (b vor a)
	if (!is_array($a) || !is_array($b)) return $b;

	// wie schauen in b nach allen keys
	foreach ($b as $k => $v){

		// der einfachste fall, einfach kopieren,
		// kein rekrusives mergen mehr notwendig
		if (!array_key_exists($k, $a)){
			$a[$k] = $v;
		}else{
			$a[$k] = array_merge_recursive_simple($a[$k], $v, $level + 1);
		}

	}
	return $a;
}

/**
 * returns striped array
 * @param array
 * @return array
 */
function stripslashesArray($r)
{
	if (is_array($r)){
		foreach ($r as $k => $v)
			$r[$k] = stripslashesArray($v);
	}else{
		$r = stripslashes($r);
	}
	return $r;
}

function array_strip($a)
{
	return stripslashesArray($a);
}

/**
 * iterates over given array. if given methodname exists on
 * list item, this method will be called. a list of return values
 * of this method is returned
 *
 * any additional parameters are passed as single parameter to the method called
 *
 * @param Array array
 * @throws Exception
 * @return Array
 */
function array_call_method($array)
{
	if (!is_array($array)) throw new Exception(__FUNCTION__." expects an array but ".var_export($array,true)." given.");
	$list = array();

	// argumente extrahieren
	$args = func_get_args();
	$array = array_shift($args);
	$method = array_shift($args);

	foreach ($array as $a){
		if (method_exists($a, $method)){
			$list[] = call_user_func_array(array(
					$a,
					$method), $args);
		}
	}
	return $list;
}

/**
 * calls a function on every value of a given array an return the results in a new array with same keys
 *
 * @param Array $array
 * @param Callback $func
 * @return Array
 */
function array_call_function($array, $func)
{
	foreach ($array as $k => $v){
		if (is_array($v)){
			$array[$k] = array_call_function($v, $func);
		}else{
			$funcs = explode(",", $func);
			foreach ($funcs as $f){
				$v = $f($v);
			}
			$array[$k] = $v;
		}
	}
	return $array;
}

/**
 * created a hashtable using a key from the values of given array without changing the value
 * (if $duplicate_as_array is set to true values will be collected in a subarray)
 *
 * @param array $array
 * @param String $fieldname
 * @param Boolean $duplicate_as_array
 * @return array
 */
function array_create_hash($array, $fieldname, $duplicate_as_array = false)
{
	$keysset = array();
	$new = array();
	foreach ($array as $v){
		if ($duplicate_as_array && array_key_exists($fieldname, $v)){
			if (array_key_exists($v[$fieldname], $keysset)){
				if ($keysset[$v[$fieldname]] == 1){
					$new[$v[$fieldname]] = array(
							$new[$v[$fieldname]]);
					$new[$v[$fieldname]][] = $v;
					$keysset[$v[$fieldname]]++;
				}else{
					$new[$v[$fieldname]][] = $v;
					$keysset[$v[$fieldname]]++;
				}
			}else{
				$new[$v[$fieldname]] = $v;
				$keysset[$v[$fieldname]] = 1;
			}
		}else{
			if (array_key_exists($fieldname, $v)) $new[$v[$fieldname]] = $v;
		}
	}
	return $new;
}

/**
 * creates a hashtable from a array
 *
 * @param array $array
 * @param String $key_fieldname
 * @param String $value_fieldname
 * @return array
 */
function array_create_simple_hash($array, $key_fieldname, $value_fieldname)
{
	$new = array();
	foreach ($array as $v){
		$new[$v[$key_fieldname]] = $v[$value_fieldname];
	}
	return $new;
}

/**
 * groups an array by a field/set of fields
 *
 * @param array $array
 * @param String|String[] $key_fieldname
 * @return array
 */
function array_group($array, $key_fieldname)
{
	if (!is_array($key_fieldname)) $key_fieldname = array($key_fieldname);
	$new = array();
	foreach ($array as $value){
		$key_value = array();
		foreach ($key_fieldname as $k) $key_value[] = $value[$k];
		$key_value = implode("-",$key_value);

		if (!isset($new[$key_value])) $new[$key_value] = array();
		$new[$key_value][] = $value;
	}
	return $new;
}

/**
 * sets a single field in every entry in a list
 * @param array $array
 * @param String $fieldname
 * @param String $value
 * @return array
 */
function array_set_value($array,$fieldname,$value)
{
	foreach (array_keys($array) as $k) {
		$array[$k][$fieldname] = $value;
	}
	return $array;
}

/**
 * @param array|\ArrayObject $arr
 * @param string $path
 * @param mixed $default
 * @return mixed
 */
function array_get_by_keypath($arr, $path, $default=null)
{
    if (!$path) return $default;

    $pathSegments = is_array($path) ? $path : explode('.', $path);

    $currentItem =& $arr;
    foreach ($pathSegments as $pathSegment) {
        if (!isset($currentItem[$pathSegment])) return $default;

        $currentItem = $currentItem[$pathSegment];
    }

    return $currentItem;
}

/**
 * @param array|\ArrayObject $arr
 * @param string $path
 * @param mixed $value
 * @return mixed
 */
function array_set_by_keypath(&$arr, $path, $value)
{
    if (!$path) return null;

    $segments = is_array($path) ? $path : explode('.', $path);
    if (is_object($arr)) $arr = (array) $arr;
    $cur =& $arr;
    foreach ($segments as $segment) {
        if (!isset($cur[$segment]))
            $cur[$segment] = array();
        $cur =& $cur[$segment];
    }
    $cur = $value;
    return $cur;
}

function array_sort_by_column(&$array,$column,$dir = SORT_ASC, $type = SORT_REGULAR)
{
	$sort = array();
	foreach ($array as $k => $v) {
		$sort[$k] = isset($v[$column]) ? $v[$column] : null;
	}
	array_multisort($sort,$dir,$type,$array);
	return $array;
}
/**
 * tests wether a string begins with a specific substring
 *
 * @param String $string
 * @param String $needle
 * @param Boolean $casesensitive
 * @return Boolean
 */
function str_begins_with($string,$needle,$casesensitive=false)
{
	if (!$casesensitive) {
		$string = strtolower($string);
		$needle = strtolower($needle);
	}
	return (strlen($string)>=strlen($needle) && substr($string,0,strlen($needle))==$needle);
}

/**
 * tests wether a string ends with a specific substring
 *
 * @param String $string
 * @param String $needle
 * @param Boolean $casesensitive
 * @return Boolean
 */
function str_ends_with($string,$needle,$casesensitive=false)
{
	if (!$casesensitive) {
		$string = strtolower($string);
		$needle = strtolower($needle);
	}
	$nLength = strlen($needle);
	$sLength = strlen($string);
	return ($sLength>=$nLength && substr($string,$sLength-$nLength,$nLength)==$needle);
}

function htmlescapeUTF8($s)
{
	return htmlentities($s,ENT_COMPAT|ENT_HTML401,"UTF-8");
}

/**
 * returns remote ip by searching in mutiple servers vars
 * @deprecated
 * @see \BF\PhpUtils\Request::get_remote_ip()
 */
function get_remote_ip()
{
    return \BF\PhpUtils\Request::get_remote_ip();
}

function array_seq($start,$end, $sameKeyAsValue=true)
{
	$a = array();
	for ($i=$start; $i<=$end; $i++) {
		if ($sameKeyAsValue) {
			$a[$i] = $i;
		}else{
			$a[] = $i;
		}
	}
	return $a;
}

function html_options($map,$selectedValue)
{
	$s = array();
	foreach ($map as $k => $v) {
		$s[] = '<option value="'.$k.'"'.($k==$selectedValue ? ' selected="selected"' : '').'>'.htmlescapeUTF8($v).'</option>';
	}

	return implode("\n",$s);
}

/**
 * a debugging function which will dump all given parameters via Var_Dump::display(),
 * prints a backtrace and exits application
 */
function debug_break()
{
    ob_start();
	echo "<pre>";
	if (func_num_args() > 0){
		$args = func_get_args();
		foreach ($args as $arg)
			var_dump($arg);
	}
	$trace = array_reverse(debug_backtrace());
	$level = 0;
	echo "</pre><hr><pre>";
	echo "<strong>debugging exit</strong><br>";
	foreach ($trace as $step){
		$pad = str_pad("", $level * 2 * 6, "&nbsp;");
		if (array_key_exists("class", $step)){
			$call = $step["class"] . "::" . $step["function"];
		}else{
			$call = $step["function"];
		}
		echo $pad . "-> $call() from " . (isset($step["file"]) ? "$step[file]:$step[line]" : "<i>unknown</i>") . "<br>";
		$level++;
	}
    $output = ob_get_contents();
    ob_end_clean();
    if (php_sapi_name()=="cli") {
        $output = str_replace("<pre>","\n",$output);
        $output = str_replace("</pre>","\n",$output);
        $output = str_replace("<br>","\n",$output);
        $output = str_replace("</strong>","**",$output);
        $output = str_replace("<strong>","**",$output);
        $output = str_replace("&nbsp;"," ",$output);
        $output = str_replace("<hr>","\n".str_pad("",72,"-")."\n",$output);
        $output = preg_replace("/\\n{2,}/","\n",$output);
    }
    echo $output;
	exit();
}

/**
 * implodes a list of items surounding with quotes and escaping given quote-character
 *
 * @param string $glue
 * @param array $parts
 * @param string $quoteChar
 * @param string $escapeChar
 * @return string
 */
function implode_query($glue,$parts,$quoteChar="'",$escapeChar='\\')
{
    $items = array();
    foreach ($parts as $part) $items[] = $quoteChar.(str_replace($quoteChar,$escapeChar.$quoteChar,$part)).$quoteChar;
    return implode($glue,$items);
}

/**
 * outputs a given list of dictionaries/objects as html table
 *
 * @param array $array
 * @param array $fieldnames optional fieldnames - would be automatically detected if set to null
 * @param array $excludedFieldnames optional list if fieldnames not to print
 * @param callback $transformer optional transformer function get 2 parameters (fieldname, value) and returns value to display in table cell
 * @param null $tableId
 */
function array_print_table($array,$fieldnames = null,$excludedFieldnames = array(), $transformer = null, $tableId = null)
{
    // first collect all available fieldnames
    if (is_null($fieldnames)) {
        $fieldnames = array();
        foreach ($array as $row) {
            $row = (array)$row;
            foreach ($row as $fieldname => $fieldvalue) {
                if (in_array($fieldname,$excludedFieldnames)) continue;
                $fieldnames[$fieldname] = $fieldname;
            }
        }
    }

    echo '<table id="'.($tableId?:'').'">';

    echo '<thead>';
    echo '<tr>';
    foreach ($fieldnames as $fieldname) {
        echo '<th>'.$fieldname.'</th>';
    }
    echo '</tr>';
    echo '</thead>';

    echo '<tbody>';

    foreach ($array as $row) {
        $row = (array)$row;
        echo '<tr>';
        foreach ($fieldnames as $fieldname) {
            if (in_array($fieldname,$excludedFieldnames)) continue;
            $value = isset($row[$fieldname]) ? $row[$fieldname] : null;
            if (!is_null($transformer)) $value = call_user_func($transformer,$fieldname,$value);
            echo '<td>'.$value.'</td>';
        }
        echo '</tr>';
    }

    echo '</tbody>';

    echo '</table>';
}

/**
 * removes ALL exception handlers!
 */
function clear_exception_handler()
{
    do {
        restore_exception_handler();
        $last = set_exception_handler(function(){});
        restore_exception_handler();
    } while($last!=null);
}

/**
 * removes all php error handlers!
 */
function clear_error_handler()
{
    do {
        restore_error_handler();
        $last = set_error_handler(function(){});
        restore_error_handler();
    }while($last!=null);
}
